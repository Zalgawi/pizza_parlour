/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaorderingapp;

import java.util.ArrayList;

/**
 *
 * @author Zayda
 */
public abstract class Pizza implements Comparable {
    
    public static final int SM_PIZZA = 12;
    public static final int MD_PIZZA = 14;
    public static final int LG_PIZZA = 16;
    public static final int XL_PIZZA = 18;
    public double[] price = {8.00, 10.00, 12.00, 14.00};
    public double cost;
    public String type;
    public ArrayList<String> toppings = new ArrayList<String>();
    public int size;

    
    
    public Pizza(){
       type = "pizza"; 
    }
    
    public Pizza(int size){
        switch(size)
        {
            case SM_PIZZA:
                cost = price[0];size = SM_PIZZA;break;
            case MD_PIZZA:
                cost = price[1];size = MD_PIZZA;break;
            case LG_PIZZA:
                cost = price[2];size = LG_PIZZA;break;
            case XL_PIZZA:
                cost = price[3];size = XL_PIZZA;break;
            default:
                System.out.println("Please pick another size");
                break;
        }
    }
    
    
    
     public abstract void getToppings();
 @Override
    public int compareTo(Object t) {
        Pizza other = (Pizza)t;
        if(this.size < other.size)
            return -1;
        else if (this.size == other.size)
            return 0;
        else 
            return 1;
    }
     public void printSring()
    {
        String topping="";
        if(toppings.size()==0)
            topping = "no toppings selected.";
        else
        {
            for(int i = 0; i<toppings.size();i++)
            {
                topping += toppings.get(i) + ", ";
            }
        }
        System.out.println("Your pizza has been ordered with the following topings: " + topping +
                " and \nThe final cost is: £"+cost);
    }
    
    
}
  
 

