/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaorderingapp;


import java.util.Scanner;


import java.util.ArrayList;

/**
 *
 * @author Zayda
 */
public class Sicilian extends Pizza {
    
    public static final int SM_WIDTH = 10; 
    public static final int MD_WIDTH = 12; 
    public static final int LG_WIDTH = 14; 
     public Sicilian() {
        
        this.type = "Sicilian";
    }
     
     public Sicilian(int size) {
        super(size);
        this.type = "Sicilian";
    }
    public double getCost()
    {
        return this.cost;
    }
    
    /**
     *  Updates the Toppings arraylist
     */
    @Override
     public void getToppings()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("How many toppings would you like?");
        int numToppings = in.nextInt();
        while(numToppings > 2)
        {
            System.out.println("You are limited to two. ");
            System.out.println("How many toppings would you like? ");
            numToppings = in.nextInt();
        }
        in.nextLine();
        for(int i = 0;i<numToppings;i++)
        {
            System.out.println("Enter topping: ");
            String topping = in.nextLine();
            toppings.add(topping);
            this.cost += 2.50;
        }
    }
     
}
